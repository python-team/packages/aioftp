Source: aioftp
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Adam Cecile <acecile@le-vert.net>
Build-Depends: debhelper-compat (= 13),
 dh-sequence-sphinxdoc <!nodoc>,
 python3-doc <!nodoc>,
 dh-python,
 pybuild-plugin-pyproject,
 python3-all,
 python3-setuptools,
 python3-sphinx <!nodoc>,
 python3-pytest <!nocheck>,
 python3-pytest-asyncio <!nocheck>,
 python3-pytest-cov <!nocheck>,
 python3-trustme <!nocheck>,
 python3-async-timeout <!nocheck>,
Standards-Version: 4.6.1
Homepage: https://github.com/aio-libs/aioftp
Vcs-Browser: https://salsa.debian.org/python-team/packages/aioftp
Vcs-Git: https://salsa.debian.org/python-team/packages/aioftp.git
Testsuite: autopkgtest-pkg-python
Rules-Requires-Root: no

Package: python3-aioftp
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Suggests: python-aioftp-doc
Description: FTP client and server for asyncio (Python 3)
 Library implementing FTP protocol, both client and server for Python asyncio
 module.
 .
 Supported commands as client: USER, PASS, ACCT, PWD, CWD, CDUP, MKD, RMD,
 MLSD, MLST, RNFR, RNTO, DELE, STOR, APPE, RETR, TYPE, PASV, ABOR, QUIT,
 REST, LIST (as fallback).
 .
 Supported commands as server: USER, PASS, QUIT, PWD, CWD, CDUP, MKD, RMD,
 MLSD, LIST (non-standard), MLST, RNFR, RNTO, DELE, STOR, RETR,
 TYPE ("I" and "A"), PASV, ABOR, APPE, REST.
 .
 This package installs the library for Python 3.

Package: python-aioftp-doc
Architecture: all
Section: doc
Depends: ${sphinxdoc:Depends}, ${misc:Depends}
Multi-Arch: foreign
Description: FTP client and server for asyncio (common documentation)
 Library implementing FTP protocol, both client and server for Python asyncio
 module.
 .
 Supported commands as client: USER, PASS, ACCT, PWD, CWD, CDUP, MKD, RMD,
 MLSD, MLST, RNFR, RNTO, DELE, STOR, APPE, RETR, TYPE, PASV, ABOR, QUIT,
 REST, LIST (as fallback).
 .
 Supported commands as server: USER, PASS, QUIT, PWD, CWD, CDUP, MKD, RMD,
 MLSD, LIST (non-standard), MLST, RNFR, RNTO, DELE, STOR, RETR,
 TYPE ("I" and "A"), PASV, ABOR, APPE, REST.
 .
 This is the common documentation package.
